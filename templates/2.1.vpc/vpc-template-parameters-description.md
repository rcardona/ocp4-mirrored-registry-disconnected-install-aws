[
  {
    "ParameterKey": "VpcCidr", (1)
    "ParameterValue": "10.0.0.0/16" (2)
  },
  {
    "ParameterKey": "AvailabilityZoneCount", (3)
    "ParameterValue": "1" (4)
  },
  {
    "ParameterKey": "SubnetBits", (5)
    "ParameterValue": "12" (6)
  }
]

(1) The CIDR block for the VPC.
(2) Specify a CIDR block in the format x.x.x.x/16-24.
(3) The number of availability zones to deploy the VPC in.
(4) Specify an integer between 1 and 3.
(5) The size of each subnet in each availability zone.
(6) Specify an integer between 5 and 13, where 5 is /27 and 13 is /19.
