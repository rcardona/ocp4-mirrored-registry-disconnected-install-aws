#!/bin/bash

source installation-env

cat > bootstrap-mapping.json << EODBootstrap
[
    {
        "DeviceName" : "/dev/sda1",
        "Ebs": { "VolumeSize" : 120, "DeleteOnTermination": false }
    }
]
EODBootstrap

cat > bootstrap-user-data << EODBootstrapUserData
{
	"ignition": {
		"config": {
			"replace": {
				"source": "http://10.0.0.5:8080/bootstrap.ign",
				"verification": {}
			}
		},
		"timeouts": {},
		"version": "2.2.0"
	},
	"networkd": {},
	"passwd": {},
	"storage": {},
	"systemd": {}
}
EODBootstrapUserData

# base64 bootstrat-user-data > bootstrat-user-data-base64.txt

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type i3.large --key-name OcpKeyPair --subnet-id ${PrivateSubnet0Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.9 --user-data file://bootstrap-user-data  > .bootstrap-creation.log
