#!/bin/bash

source installation-env

cat > master-mapping.json << EODMaster
[
    {
        "DeviceName" : "/dev/xvda",
        "Ebs": { "VolumeSize" : 120, "VolumeType": "gp2", "DeleteOnTermination": false }
    }
]
EODMaster

cat > master-user-data << EODMasterpUserData
{
	"ignition": {
		"config": {
			"append": [{
				"source": "https://api-int.ocpcluster.testo.labo:22623/config/master",
				"verification": {}
			}]
		},
		"security": {
			"tls": {
				"certificateAuthorities": [{
					"source": "data:text/plain;charset=utf-8;base64,LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURFRENDQWZpZ0F3SUJBZ0lJRlZuZUd2Wms5UGN3RFFZSktvWklodmNOQVFFTEJRQXdKakVTTUJBR0ExVUUKQ3hNSmIzQmxibk5vYVdaME1SQXdEZ1lEVlFRREV3ZHliMjkwTFdOaE1CNFhEVEl3TVRBeU1EQXhORGd3TjFvWApEVE13TVRBeE9EQXhORGd3TjFvd0pqRVNNQkFHQTFVRUN4TUpiM0JsYm5Ob2FXWjBNUkF3RGdZRFZRUURFd2R5CmIyOTBMV05oTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFwMmx4SkZBc001a3EKSStQdHRLQlhnYThUejYrQ1NSc1dPcVRRTy9GbSsvTEEyaVdEd3FQQkpjeXNUYitST2d1U3UzNGFYWGJKWDRRQQpsZjJBUVpnNUlZNlJTMmdwczNmbUZOdldodTFNS0s2VHVYNnF2WlIwOTBySmNCREtjYlgxbEZWcGxlL3NMbGNGCkpTRzNLcUZaN2xVM1BoZ2l4dnhMaWNmTkxDS1kzcEtac1o0bG9ocWtHTDdWUFpqVXJUS1RnVWlYOU1GNVpWRFQKa0FZQTdNQUVJMi8zaXNJSDR4YVZQcmpkbTVXYjNVUk9oeGVOb3lLb1J5RzcxbEJqTDE5Y0JNMmQ0UXl0Zm1jMgppcGR1QjdWYmNmVENPTHhTbkdheWhibjJGMjRJZ3B3M3lwUGtQeDR6Zkk3N0YvVHZhNkRqYjNvdE1pbTJMNlRDCllKWFg4aWdQWndJREFRQUJvMEl3UURBT0JnTlZIUThCQWY4RUJBTUNBcVF3RHdZRFZSMFRBUUgvQkFVd0F3RUIKL3pBZEJnTlZIUTRFRmdRVWltMFhBWUhkdkwzMm1seDlySkxLVk1NVFlrMHdEUVlKS29aSWh2Y05BUUVMQlFBRApnZ0VCQUFRYzlBU2pQckduNEx1dEUwd1I4SkVqRE82L2ZhZzA1U2JLUTF6MmwrdXlRY2VqY0kzKzh6dXp3UGQwClFmRGI1L3FXOXdsTlU4RHJtYTdOc05UUkxidXhIWHRobUFlTWU3ejI2Tk5qQnVidU04SGxKK1dCUUNjMjVnSEcKN0dUclpDWTVZcFVQWDdwWTVUbnRQMm4zQnhCOENQWU9vK1k1OExpWGRwYWF4K0VuZC9TbVRzK0hnTjd6UEd4QQpET2pFejUyU3JrWndWM2x2Zk9DaEZacVZmaFd0SkJoQnExclZHZEw2VGVZV3J3dWhzVWxiUDliU1ZGNkdhT3grCnlKcm5TcFVTN21qdFAwUFR6OEFrQWh4ODl0MzZyZnVFcmV2cm5pZ21lcmhQcW1Xd1FoS3R6YnJsZ2p0RGw1NFIKZkhBVEhJTW1NMFMvMnpQWG55TG42aU1EbDJJPQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg==",
					"verification": {}
				}]
			}
		},
		"timeouts": {},
		"version": "2.2.0"
	},
	"networkd": {},
	"passwd": {},
	"storage": {},
	"systemd": {}
}
EODMasterpUserData

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet0Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone0} --private-ip-address 10.0.0.10 --user-data file://master-user-data > .master-0-creation.log

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet1Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone1} --private-ip-address 10.0.1.10 --user-data file://master-user-data > .master-1-creation.log

aws ec2 run-instances --image-id ami-0d2e5d86e80ef2bd4 --instance-type m4.xlarge --key-name OcpKeyPair --block-device-mappings file://master-mapping.json --subnet-id ${PrivateSubnet2Id} --security-group-ids ${MasterSecurityGroupId} ${ClusterSecurityGroupId} --placement AvailabilityZone=${AvailabilityZone2} --private-ip-address 10.0.2.10 --user-data file://master-user-data > .master-2-creation.log

